<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Pessoa;
use Illuminate\Http\Request;

class PessoasController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return View
     */
    public function list()
    {
        $pessoas = Pessoa::all();

        return view('pessoas.list')
            ->with('pessoas', $pessoas);
    }

    /**
     * Show the profile for the given user.
     *
     * @return View
     */
    public function add()
    {

        return view('pessoas.add');
    }

    public function store(Request $request){

        $data = $request->input();

        if(Pessoa::create($data)){
            return redirect()->route('pessoas.list')
                ->with('success', 'Pessoa cadastrada com sucesso');
        }

        return redirect()->route('pessoas.list')
            ->with('error', 'Pessoa não foi cadastrada');

    }

    public function delete(Request $request){

        $data = $request->input();

        $pessoa = Pessoa::find($data['id']);

        if($pessoa->delete()){
            return redirect()->route('pessoas.list')
                ->with('success', 'Pessoa deletada com sucesso');
        }

        return redirect()->route('pessoas.list')
            ->with('error', 'Pessoa não foi deletada');

    }
}
