@extends('layouts.app')

@section('title', 'Cadastra Pessoas')


@section('content')

    <div class="row mt-3">
        <div class="col-md-12">
            <form method="post" action="{{ route('pessoas.store') }}">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Nome</label>
                    <input type="text" class="form-control" placeholder="Nome" name="nome">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Telefone</label>
                    <input type="text" class="form-control" placeholder="Telefone" name="telefone">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Endereço</label>
                    <input type="text" class="form-control" placeholder="Endereço" name="endereco">
                </div>
                <button class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection
