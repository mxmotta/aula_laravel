@extends('layouts.app')

@section('title', 'Pessoas')


@section('content')


    <div class="row mt-3">
        <div class="col-md-12 text-right">
            <a href="{{ route('pessoas.add') }}" class="btn btn-success">Cadastrar nova</a>
        </div>
    </div>

    <table class="table mt-3">
        <thead>
            <th>ID</th>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Email</th>
            <th>Endereço</th>
            <th>Data cadastro</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($pessoas as $pessoa)
                <tr>
                    <td>{{ $pessoa->id }}</td>
                    <td>{{ $pessoa->nome }}</td>
                    <td>{{ $pessoa->telefone }}</td>
                    <td>{{ $pessoa->email }}</td>
                    <td>{{ $pessoa->endereco }}</td>
                    <td>{{ \Carbon\Carbon::parse($pessoa->created_at)->format('d/m/Y h:i') }}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalExcluir" data-id="{{ $pessoa->id }}">Deletar</button>
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Excluir Pessoa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formDelete" action="{{ route('pessoas.delete') }}" method="post">
                        @csrf
                        <input type="hidden" name="id">
                    </form>
                    Deseja realmente excluir essa pessoa?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="botaoEnviar" type="button" class="btn btn-danger">Excluir</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>
        $('#modalExcluir').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');

            var modal = $(this);
            modal.find('.modal-body input[name="id"]').val(id);
        });

        $('#botaoEnviar').on('click', function () {
            $('#formDelete').trigger('submit');
        });
    </script>

@endpush
