<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/pessoas/', 'PessoasController@list')->name('pessoas.list');
Route::get('/pessoas/add', 'PessoasController@add')->name('pessoas.add');
Route::post('/pessoas/store', 'PessoasController@store')->name('pessoas.store');
Route::post('/pessoas/delete', 'PessoasController@delete')->name('pessoas.delete');
